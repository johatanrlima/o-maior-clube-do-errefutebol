# a ser editador pelo utilizador
import PESOS
# for performing your HTTP requests
import requests
# for xml & html scrapping
from bs4 import BeautifulSoup

class Clube:
    def __init__(self):
        # nome do clube
        self.nome = "Só Corno FC"
        # nacional
        self.num_liga_nacional = 0
        self.num_copa_nacional = 0
        self.num_outras_nacional = 0
        # continental
        self.num_libertadores_continental = 0
        self.num_sulamericana_continental = 0
        self.num_outras_continental = 0
        # mundial
        self.num_copa_mundial = 0
        self.num_outras_mundial = 0

    def peso_total_conquistas(self):
        total = 0
        # nacional
        total += self.num_liga_nacional * PESOS.PESO_LIGA_NACIONAL
        total += self.num_copa_nacional * PESOS.PESO_COPA_NACIONAL
        total += self.num_outras_nacional * PESOS.PESO_OUTRAS_NACIONAL
        # continental
        total += self.num_libertadores_continental * PESOS.PESO_LIBERTADORES_CONTINENTAL
        total += self.num_sulamericana_continental * PESOS.PESO_SULAMERICANA_CONTINENTAL
        total += self.num_outras_continental * PESOS.PESO_OUTRAS_CONTINENTAL
        # mundial
        total += self.num_copa_mundial * PESOS.PESO_COPA_MUNDIAL
        total += self.num_outras_mundial * PESOS.PESO_OUTRAS_MUNDIAL

        return total

    def mostrar_valores(self):
        print("---")
        for x in self.__dict__:
            print(x, ":", self.__dict__[x])


# https://stackoverflow.com/questions/24516340/replace-all-occurrences-of-item-in-sublists-within-list
def nestrepl(lst, what, repl):
    for index, item in enumerate(lst):
        if type(item) == list:
            nestrepl(item, what, repl)
        else:
            if item == what:
                lst[index] = repl



# url of wikipedia page from which you want to scrap tabular data.
url1 = "https://pt.wikipedia.org/wiki/Lista_de_n%C3%BAmero_de_t%C3%ADtulos_conquistados_por_times_brasileiros_de_futebol"

s = requests.Session()
response = s.get(url1, timeout=10)

soup = BeautifulSoup(response.content, 'html.parser')
tables = soup.findAll('table')
rows = []
for table in tables:
    rows += table.findAll("tr")

todos_dados_tabela = []
for row in rows:
    if row != []:
        todos_dados_tabela.append([td.text.rstrip() for td in row.find_all('td')])

total_clubes_wikipedia = []
for clube in todos_dados_tabela:
    if len(clube) == 13:
        total_clubes_wikipedia.append(clube)

nestrepl(total_clubes_wikipedia, '-', '0')
# quando construímos um objecto, recebemos uma lista assim:
# [' São Paulo', '6', '-', '-', '6', '3', '3', '3', '9', '3', '-', '3', '18']
# estes valores respondem aos totais de títulos ganhos

total_clubes_da_classe = []
for clube_wikipedia in total_clubes_wikipedia:
    #print(clube_wikipedia)
    clube_da_classe = Clube()
    clube_da_classe.nome = clube_wikipedia[0].strip()
    clube_da_classe.num_liga_nacional = int(clube_wikipedia[1])
    clube_da_classe.num_copa_nacional = int(clube_wikipedia[2])
    clube_da_classe.num_outras_nacional = int(clube_wikipedia[3])
    # saltamos o 4 porque é o total nacional
    clube_da_classe.num_libertadores_continental = int(clube_wikipedia[5])
    clube_da_classe.num_sulamericana_continental = int(clube_wikipedia[6])
    clube_da_classe.num_outras_continental = int(clube_wikipedia[7])
    # saltamos o 8 porque é o total continental
    clube_da_classe.num_copa_mundial = int(clube_wikipedia[9])
    clube_da_classe.num_outras_mundial = int(clube_wikipedia[10])

    total_clubes_da_classe.append(clube_da_classe)

# for clube in total_clubes_da_classe:
#     clube.mostrar_valores()

# lista de dicionários para mostrar
unsorted_dict = {}
for clube in total_clubes_da_classe:
    #print(clube.nome, "|", clube.peso_total_conquistas())
    unsorted_dict[clube.nome] = clube.peso_total_conquistas()

clubes_ordenados = {k: v for k, v in sorted(unsorted_dict.items(), key=lambda item: item[1], reverse=True)}
# apresentação da pasta
print("Posição | Nome do Clube | Peso das Conquistas")
print(":--|:--|--:")
i = 0
valor_anterior = 0
for x in clubes_ordenados:
    if clubes_ordenados[x] != valor_anterior:
        i += 1
        valor_anterior = clubes_ordenados[x]
    print(i, '|', x,'|' , clubes_ordenados[x])


# visualização em gráfico
import matplotlib.pylab as plt
lists = (clubes_ordenados.items()) # sorted by key, return a list of tuples
x, y = zip(*lists) # unpack a list of pairs into two tuples
plt.xticks(rotation=40)

plt.plot(x, y)
plt.show()
