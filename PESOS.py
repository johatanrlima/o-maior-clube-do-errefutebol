# copiado daqui dentro do possivel:
# https://pt.m.wikipedia.org/wiki/Ranking_Folha

# nacionais
PESO_LIGA_NACIONAL   = 25
PESO_COPA_NACIONAL   = 15
PESO_OUTRAS_NACIONAL = 5

# continentais
PESO_LIBERTADORES_CONTINENTAL = 35
PESO_SULAMERICANA_CONTINENTAL = 15
PESO_OUTRAS_CONTINENTAL       = 5

# mundiais
PESO_COPA_MUNDIAL   = 25
PESO_OUTRAS_MUNDIAL = 1
