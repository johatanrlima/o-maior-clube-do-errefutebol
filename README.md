# todo:

falta adicionar estaduais

# dependencias:
python3 (feito com Python 3.8.5)

pip3

modulo BeautifulSoup

# inputs:

PESOS.py

tabela da wikipedia

# outputs:

tabela ordenada dos maiores clubes brasileiros pronta para copypaste no reddit

# links

## relevante para a versão 0.1
https://sateesh110.medium.com/how-to-scrape-wikipedia-table-using-python-beautiful-soup-cd0d8ee1a319

https://pt.wikipedia.org/wiki/Lista_de_n%C3%BAmero_de_t%C3%ADtulos_conquistados_por_times_brasileiros_de_futebol#Por_clube

## possiveis fontes de dados futuras

http://www.foot.dk/Conmebolklubber.asp?NatID=3

https://www.campeoesdofutebol.com.br/hist_clubes.html

http://www.futebol80.com.br/



# exemplos

## pesos brasileiros

feito com este ficheiro:

https://gitlab.com/aqrdt/o-maior-clube-do-errefutebol/-/blob/0.1-pesos-brasileiros/PESOS.py


Posição | Nome do Clube | Peso das Conquistas
:--|:--|--:
1 | São Paulo | 210.0
2 | Santos | 188.0
3 | Palmeiras | 165.0
4 | Flamengo | 151.0
5 | Corinthians | 150.0
6 | Cruzeiro | 145.0
7 | Grêmio | 127.0
8 | Internacional | 106.0
9 | Vasco da Gama | 90.0
10 | Atlético Mineiro | 61.0
11 | Fluminense | 48.0
12 | Botafogo | 32.0
13 | Atlético Paranaense | 30.0
14 | Bahia | 20.0
15 | Sport | 17.0
16 | Chapecoense | 12.0
17 | Coritiba | 10.0
18 | Guarani | 10.0
19 | Criciúma | 7.0
20 | Juventude | 7.0
21 | Santo André | 7.0
22 | Paulista | 7.0
23 | Paysandu | 1.0
24 | Paulistano | 1.0
25 | America | 1.0
26 | Maringá | 1.0


## pesos europeus

feito com este ficheiro:

https://gitlab.com/aqrdt/o-maior-clube-do-errefutebol/-/blob/0.1-pesos-europeus/PESOS.py


Posição | Nome do Clube | Peso das Conquistas
:--|:--|--:
1 | Palmeiras | 156.0
2 | São Paulo | 147.0
3 | Santos | 146.0
4 | Cruzeiro | 133.0
5 | Flamengo | 127.0
6 | Corinthians | 117.0
7 | Grêmio | 103.0
8 | Internacional | 82.0
9 | Vasco da Gama | 81.0
10 | Atlético Mineiro | 52.0
11 | Fluminense | 48.0
12 | Botafogo | 29.0
13 | Atlético Paranaense | 27.0
14 | Bahia | 20.0
15 | Sport | 17.0
16 | Coritiba | 10.0
17 | Guarani | 10.0
18 | Chapecoense | 9.0
19 | Criciúma | 7.0
20 | Juventude | 7.0
21 | Santo André | 7.0
22 | Paulista | 7.0
23 | Paysandu | 1.0
24 | Paulistano | 1.0
25 | America | 1.0
26 | Maringá | 1.0

## pesos ranking folha (aproximado)
Posição | Nome do Clube | Peso das Conquistas
:--|:--|--:
1 | Santos | 450
2 | São Paulo | 435
3 | Palmeiras | 415
4 | Corinthians | 345
5 | Flamengo | 340
6 | Cruzeiro | 305
7 | Grêmio | 285
8 | Internacional | 255
9 | Vasco da Gama | 230
10 | Fluminense | 145
11 | Atlético Mineiro | 120
12 | Atlético Paranaense | 85
13 | Botafogo | 65
14 | Bahia | 50
15 | Sport | 40
16 | Coritiba | 25
16 | Guarani | 25
17 | Criciúma | 15
17 | Juventude | 15
17 | Santo André | 15
17 | Paulista | 15
17 | Chapecoense | 15
18 | Paysandu | 5
18 | Paulistano | 5
18 | America | 5
18 | Maringá | 5
